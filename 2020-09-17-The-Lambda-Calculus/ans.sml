(*Q1*)
type var = string
datatype expr= var 
         |Ap of expr*expr
         |Ab of var*expr

(*Q2*)               
fun hr (xs,n_s,p)=case xs of
        [] => !n_s
        | y::ys =>(p := !p+1;(
    
                (
            case String.sub(String.map Char.toUpper y,!p) of
            #"Z"=> (n_s:= !s^String.str(#"A")^helper (ys,n_s,p);!s)
| _ => (n_s:= !n_s^(String.str(Char.succ(String.sub(String.map Char.toUpper y,!p))))^hr (ys,n_s,p);!n_s)
                 )
                          )
              )
                
                  
fun fresh xs=let
    val p=ref ~1;
    val n_ s=ref "";
in
    helper (xs,n_s,p)
end

(*Q3*)
* Qn 1 *)
fun delete (s,[])     = []
  | delete (s,x::xs') = 
    if s = x then delete(s,xs') 
    else x::delete(s, xs')

datatype expr= var of string
         |Apl of expr*expr
         |Abs of string*expr
    
fun free (e:expr)=case e of
            var x=>[x]
         | Ap(e1,e2) =>free e1 @ free e2
         | Ab(y,ey) =>delete(y,free ey) 


