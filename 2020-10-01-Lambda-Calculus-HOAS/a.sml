(* normal representation of lambda calculus *)
datatype lam = V of string
             | A of lam    * lam
             | L of string * lam

(* HOAS representation of lambda calculus *)

datatype hlam = HV   of string
              | HA   of hlam * hlam
              | HL   of hlam -> hlam

(* as opposed to L of string * lam *)
(*1*)


fun subst (p,y) (HV b ) = if b = p then y else HV b
  | subst (p,y) (HA(b1,b2)) = HA (subst (p,y) b1, subst (p,y) b2)
  | subst (p,y) (HL f) =
    let
	fun fp inp = subst (p,y) (f inp)
    in
	HL fp
    end
(*2*)

fun abst p mh =
    let
	fun f nh = subst (p, nh) mh
    in
	HL f
    end

(*4*)
fun hoas (V p) = HV p          
  | hoas (A(x,y)) = HA (hoas x, hoas y)
  | hoas (L(a,b)) = abst a (hoas b)

fun s (HV p) = V p
  | s (HA(x,y)) = A(s a, s b)
