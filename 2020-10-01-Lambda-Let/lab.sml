(*Q1*)

 type v = string
datatype e = var of v
         |compose of e*e
         |lmd of v*e
	

datatype lambda_let = Let of v*e*e
datatype lambda_letrec = Letrec of (v*e)list * e

(*Q2*)
fun unletrec(Letrec([(a,x1)],x2)) =
    let
    fun L m =compose((l(a,compose(m,compose(var(a),var(a))))),(l(a,compose(var(a),var(a))))))
    in

    (Let(a,x1,(L(l(a,x2)))))
        end

fun unlet (Let(y,x1,x2)) = compose(l(y,x2),x1)

