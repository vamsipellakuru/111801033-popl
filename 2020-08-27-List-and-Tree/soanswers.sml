(*Q1*)
fun map f nil = nil
  | map f(x::xs) = f x :: map f xs;

(*Q2*)
datatype 'a tree = empty
		  |node of 'a * 'a tree * 'a tree;
(*Q3*)
fun treemap f empty = empty 
  | treemap f (node(root,ltree,rtree)) = node(f root,treemap f ltree, treemap frtree);
(*Q4*)
(*preorder*)
fun preorder empty = nil
  | preorder (node(root,ltree,rtree)) = [root]@preorder (ltree) @preorder(rtree);

(*inorder*)
fun inorder empty = nil
  | inorder  (node(root,ltree,rtree)) = inorder(ltree)@[root]@ inorder (rtree);

(*postorder*)
fun postorder empty = nil
  | postorder (node(root,ltree,rtree)) = postorder(ltree)@ postorder (rtree)@[root];

(*Q5*)
 fun CWRotation (node(a, node(b,t1,t2),t3))= node(b,t1,node(a,t2,t3))
   | CWRotation t = t;
					 
