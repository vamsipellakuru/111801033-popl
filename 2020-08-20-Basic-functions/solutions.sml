(*
LAB-2
PELLAKURU VAMSI
111801033
*)
(*1.Write the tri-variate (i.e. 3 variable) versions of curry and uncurry functions. First step is to write down the type (in the comments *)
(*
val curry = fn : ('a * 'b * 'c -> 'd) -> 'a -> 'b -> 'c -> 'd
val uncurry = fn : ('a -> 'b -> 'c -> 'd) -> 'a * 'b * 'c -> 'd
*)


fun curry f x y z = f (x,y,z);
fun uncurry f (x,y,z) = f x y z;


(*2.Write the functions fst : 'a * 'b -> 'a and snd : 'a * 'b -> 'b that project a tuple into its components. *)


fun first (x,_) = x;
fun second (_,y) = y;


(*3.Write the length function for lists length : 'a list -> int. *)

fun length nil =0
  | length (x::xy) = 1 + (length xy);


(*4.Write the reverse : 'a list -> 'a list function. Be careful to not make it O(n^2) *)


fun reverse nil = nil
  | reverse (x::xy) =(reverse xy) @ [x];


(*5.Write a function to compute the nth element in the Fibonacci sequence fib : int -> int. Be careful for the obvious version is exponential. *)

fun temp 0 x y = x
  | temp 1 x y = y
  | temp n x y = temp (n-1) y (x+y);

fun fibonacci n = temp n 0 1;
