(* 1 *)
signature COUTR = sig
    val incr : unit -> unit
    val decr : unit ->unit
    val get : unit -> int
end

structure Counter : COUTR = struct
    val a = ref 0
    fun incr () = (a := !a +1)
    fun decr () = (a := !a -1)
    fun get () = !a
end
val x = Counter.incr()
val y = Counter.get()
val z = Counter.decr()
val w = Counter.get()
(* 2 *)

functor MkCounter () : COUTR = struct
    val c       = ref 0
    fun incr ()  = (a := !a +1)
    fun decr () = (a := !a -1)
    fun get ()   = !a
end
structure A = MkCounter ()
structure B = MkCounter ()
val p = A.incr()
val q = B.decr()
val r = A.get()
val s = B.get()
