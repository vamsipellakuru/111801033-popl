(*Q1*)
(*foldr : ('elem * 'summary -> 'summary) -> 'summary -> 'a list -> 'summary *)
fun foldr(sfun : 'a * 'b -> 'b) (s0:'b) [] = s0
  | foldr sfun s0 (x::xs) = sfun(x,(foldl sfun s0 xs));
(*foldl : ('elem * 'summary -> 'summary) -> 'summary -> 'a list -> 'summary *)
fun foldl(sfun : 'a * 'b -> 'b) (s0:'b) [] = s0
  | foldl sfun s0 (x::xs) = foldl sfun (sfun(x,s0)) xs;
(*Q2*)
fun sum (l: int list) : int = foldl (fn (x,s0)=>x+s0) 0 l;
(*Q3*)
(*partition : ('a -> bool) -> 'a list -> 'a list * 'a list*)
fun partition fnt l = let fun f1 (k,(l,y))=if f k then (k::l,y)
					   else (l,k::y) in foldr f1([],[]) l end;
(*map : ('a -> 'b) -> 'a list -> 'b list.*)
fun map fnt l =let fun f1(k,l) = fnt k::l in foldr f1 [] l end;
(*reverse : 'a list -> 'a list*)
fun reverse l = let fun f1(k,l)=k::l in foldl f1 [] l end;
(*nth : 'a list * int -> 'a option.*)

